console.log("Hello World!");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"] 
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
};

console.log(trainer);

// Access object properties using dot notation
console.log("Results of the dot notation:");
console.log(trainer.name);

// Access object properties using brackets
console.log("Result of the bracket notation:");
console.log(trainer['pokemon']);

// Invoke the talk object method
console.log("Result of the talk notation:");
trainer.talk();

// Create a constructor for creating a pokemon

function Pokemon(name,level) {

	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;
	this.tackle = function(pokemon){

		pokemon.health = pokemon.health - this.attack;

		console.log(this.name + " tackled " + pokemon.name);

		if(this.health <= 0){
			console.log(this.name + "\'s health is 0. Please select another pokemon");
		} else if(pokemon.health >= 0 ) {
			console.log(pokemon.name + " health dropped to " + pokemon.health);
		} else {
			console.log(pokemon.name + "\'s health is 0.");
			pokemon.faint();
		};

	};

	this.faint = function(){

		if(this.health <= 0) {
			console.log(this.name + " has fainted!");
		} else {
			console.log(this.name + " still has " + this.health + " hp.");
		};

	};

}

let pokemon1 = new Pokemon("Pikachu",45);
console.log(pokemon1);

let pokemon2 = new Pokemon("Charizard",66);
console.log(pokemon2);

// Attack
pokemon1.tackle(pokemon2);
pokemon1.tackle(pokemon2);
pokemon1.tackle(pokemon2);
pokemon2.tackle(pokemon1);

// Faint - checks first if hp is 0 before confirming if pokemon fainted
pokemon1.faint();